package daz.lmis_light;

import android.os.AsyncTask;
import android.util.Log;

import com.github.kevinsawicki.http.HttpRequest;


/**
 * Created by aman on 26.11.15.
 */
public class HttpLoginClient  extends AsyncTask<String,Void, JSONResponse> {


    @Override
    protected JSONResponse doInBackground(String... params) {

        JSONResponse jsonResponse = new JSONResponse();
        HttpRequest request = HttpRequest.get(params[2]).contentType("application/json").basic(params[0],params[1]);

        if(request.ok())
        {
            jsonResponse.setStatus(200);
            Log.d("D"," User Logged ");
            return  jsonResponse;
        }else
        {
            jsonResponse.setStatus(404);
            return jsonResponse;

        }

    }
}