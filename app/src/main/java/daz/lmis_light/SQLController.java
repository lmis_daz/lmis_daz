package daz.lmis_light;
/**
 * Created by Devesh on 18/11/15.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class SQLController {

    private MyDbManager dbManager;
    private Context context;
    private SQLiteDatabase db;

    public SQLController(Context c) {
        context = c;
    }

    public SQLController open() throws SQLException {
        dbManager = new MyDbManager(context);
        db = dbManager.getWritableDatabase();
        return this;
    }

    public void close() {
        dbManager.close();
    }



    public void addData(String commodityName, String quantity) {
        ContentValues cv = new ContentValues();
        cv.put(MyDbManager.COMMODITY_NAME, commodityName);
        cv.put(MyDbManager.QUANTITY, quantity);
        db.insert(MyDbManager.TABLE_COMMODITY, null, cv);


    }

    public String databaseToString(){
        String dbString = "";
        SQLiteDatabase db1 = dbManager.getWritableDatabase();
        String query = "SELECT * FROM " + MyDbManager.TABLE_COMMODITY + " WHERE 1";
        Cursor c = db1.rawQuery(query, null);
        c.moveToFirst();
        while (!c.isAfterLast()) {
            if (c.getString(c.getColumnIndex("commodityName")) != null && c.getString(c.getColumnIndex("quantity")) != null) {
                dbString += c.getString(c.getColumnIndex("commodityName"));
                dbString += "\t:\t";
                dbString += c.getString(c.getColumnIndex("quantity"));
                dbString += "\n";
            }
            c.moveToNext();
        }
        db1.close();
        return dbString;
    }

    public Cursor readData() {
        String[] allColumns = new String[] {MyDbManager.COMMODITY_ID, MyDbManager.COMMODITY_NAME,
                MyDbManager.QUANTITY };
        Cursor c = db.query(MyDbManager.TABLE_COMMODITY, allColumns, null, null, null,
                null, null);
        if (c != null) {
            c.moveToFirst();
        }

        return c;
    }


    public void deleteDataById(int commodityId) {
        db.execSQL("DELETE FROM " + MyDbManager.TABLE_COMMODITY + " WHERE " + MyDbManager.COMMODITY_ID +
                "=\"" + commodityId + "\";");
    }

    public void updateCommodity(int cId, String cName, String qnt ) {
        String UpdateSQL = "Update " + MyDbManager.TABLE_COMMODITY + " set commodityName = '" + cName + "', quantity= '" + qnt + "' WHERE commodityId = " +  cId + ";";
        System.out.println(UpdateSQL);
        db.execSQL(UpdateSQL);
    }

    public Commodity getCommodityById(int commodityId) {
        Commodity commodity = new Commodity();
        Cursor c = db.rawQuery("SELECT commodityId, commodityName, quantity FROM " + MyDbManager.TABLE_COMMODITY + " WHERE commodityId = " +  commodityId ,null);
        if (c != null ) {
            if  (c.moveToFirst()) {
                String cName =  c.getString(c.getColumnIndex("commodityName"));
                String quantity =  c.getString(c.getColumnIndex("quantity"));
                commodity.set_commodityName(cName);
                commodity.set_quantity(quantity);
            }
        }
        return commodity;
    }

    public void deleteAllData() {
        db.execSQL("DELETE FROM " + MyDbManager.TABLE_COMMODITY); //delete all rows in a table
        db.close();
    }

}