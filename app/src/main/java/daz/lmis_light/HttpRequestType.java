package daz.lmis_light;

/**
 * Created by aman.
 */
public enum HttpRequestType {

    GET,POST,DELETE,OPTIONS,HEAD
}
