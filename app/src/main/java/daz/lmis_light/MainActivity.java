package daz.lmis_light;


import android.app.Activity;
import android.app.AlertDialog;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TableRow.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import java.util.Map;


public class MainActivity extends Activity {

    TableLayout table_layout;
    EditText ETcommodityName, ETquantity, subject;
    SQLController sqlController;

    Spinner spinner;
    String selectedUser;
    RadioGroup radioGroup;
    String enterSubject;

    private  String userName;
    private  String passwd;
    private  String recipientType;
    private String id;
    private  List<String> names;
    private  Map<String,String> nameIdMapper;
    private HttpClientTask clientTask;
    int selection = 0;

    private int comIdForUpdate;


    ///////////////////////// Methods /////////////////////////////////


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sqlController = new SQLController(this);
        sqlController.open();
        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        subject = (EditText) findViewById(R.id.subject);
        ETcommodityName = (EditText) findViewById(R.id.ETcommodityName);
        ETquantity = (EditText) findViewById(R.id.ETquantity);
        table_layout = (TableLayout) findViewById(R.id.tableLayout1);
        Bundle bundle = getIntent().getExtras();
        setUserName(bundle.get("user").toString());
        setPasswd(bundle.get("password").toString());

        makeTable();
    }

    public void selectUser(){
        List<String> users = this.getNames();
        spinner = (Spinner) findViewById(R.id.user_spinner);
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, users);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                spinner.setSelection(position);
                selectedUser = (String) spinner.getSelectedItem();
                setId(nameIdMapper.get(selectedUser));

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }
        });
    }

    public void user_Clicked(View view){
        selection = 1;
        showtoast("User Selected");
        setRecipientType("users");
        getDhisRecipients("users");
        selectUser();
    }

    public void userGroup_Clicked(View view){
        selection = 1;
        setRecipientType("userGroups");
        showtoast("User Group Selected");
        getDhisRecipients("userGroups");
        selectUserGroup();
    }

    public void orgUnit_Clicked(View view){
        selection = 1;

        setRecipientType("organisationUnits");
        showtoast("Organisation Unit Selected");
        getDhisRecipients("organisationUnits");
        selectOrganisationUnit();
    }

    public void btn_addClicked(View view){
        table_layout.removeAllViews();
        String commodityName = ETcommodityName.getText().toString();
        String quantity = ETquantity.getText().toString();
        if(commodityName.isEmpty() || quantity.isEmpty()){
            if(commodityName.isEmpty() && quantity.isEmpty())
                showMessage("Please Enter","Commodity Name & Quantity");
            else if(commodityName.isEmpty())
                showMessage("Please Enter","Commodity Name");//show message
            else if(quantity.isEmpty())showMessage("Enter","Quantity");
        }else{
            sqlController.open();
            sqlController.addData(commodityName, quantity);
            showtoast(commodityName + " added");
            ETcommodityName.setText("");
            ETquantity.setText("");
        }
        makeTable();
    }


    public void btn_UpdateClicked(View view){
        table_layout.removeAllViews();
        String commodityName = ETcommodityName.getText().toString();
        String quantity = ETquantity.getText().toString();
        sqlController.open();
        sqlController.updateCommodity(comIdForUpdate, commodityName, quantity);
        sqlController.close();
        //ResetForm();
        makeTable();
        showtoast("Commodity Updated");
    }


    public void btn_deleteClicked(View view){
        table_layout.removeAllViews();
        sqlController.open();
        sqlController.deleteAllData();
        showtoast("All commodities deleted");
        ETcommodityName.setText("");
        ETquantity.setText("");
        makeTable();
    }

    public void btn_sendClicked(View view){
        enterSubject = subject.getText().toString();
        sqlController.open();
        Cursor c = sqlController.readData();
        if(c.getCount()==0){
            showMessage("Error","No data found");//show message}
        }else if(selection==0){
            showMessage("Please specify recipient from:","User, User Group, Organisation Unit");//show message}
        }else if(enterSubject.isEmpty()){
            showMessage("Please Enter","Subject");//show message}
        }else{
            sendMessageToDhis(sqlController.databaseToString(),enterSubject);
        }
    }

    private void sendMessageToDhis(String input,String subject) {
        JSONResponse jsonResponse;
        clientTask =  new HttpClientTask();
        try {
            clientTask.setProgramTaskType(ProgramTaskType.POSTMESSAGE);
            clientTask.setRequestType(HttpRequestType.POST);
            jsonResponse = clientTask.execute(input, this.getId(), getUserName(), getPasswd(), subject, recipientType).get();
            if(jsonResponse.getStatus() == 200 || jsonResponse.getStatus() == 201)
            {
                showtoast("Message Sent to: " + selectedUser);

            }
        }catch (Exception e){}
    }

    private void getDhisRecipients(String userType) {

        clientTask =  new HttpClientTask();
        try {

            clientTask.setProgramTaskType(ProgramTaskType.GETRECIPIENT);
            clientTask.setRequestType(HttpRequestType.GET);
            JSONResponse jsonResponse = clientTask.execute(userType,getUserName(),getPasswd()).get();
            names = jsonResponse.getDataPopulator().getUserNames();
            nameIdMapper = jsonResponse.getDataPopulator().getUserIDMapper();

        }catch (Exception e){
            Log.d("D",e.getLocalizedMessage() );
        }
    }

    public void showtoast(String message) {
        Toast toast = Toast.makeText(this, message, Toast.LENGTH_SHORT);
        toast.show();
    }

    public void showMessage(String title, String Message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(Message);
        builder.show();
    }

    private void makeTable() {
        sqlController.open();
        Cursor c = sqlController.readData();
        int rows = c.getCount();
        int cols = c.getColumnCount();
        c.moveToFirst();
        for (int i = 0; i < rows; i++) {
            TableRow row = new TableRow(this);
            row.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
            for (int j = 1; j < cols; j++) {
                TextView tv = new TextView(this);
                if(j==1){
                    tv.setLayoutParams(new LayoutParams(400, LayoutParams.WRAP_CONTENT));
                }
                if(j==2){
                    tv.setLayoutParams(new LayoutParams(300, LayoutParams.WRAP_CONTENT));
                }
                tv.setGravity(Gravity.LEFT);
                tv.setTextSize(16);
                row.setPadding(0, 0, 0, 5);
                tv.setText(c.getString(j));
                row.addView(tv);

            }

            int commodityId = Integer.parseInt(c.getString(0));
            //Create Edit Button
            Button buttonEdit = new Button(this);
            buttonEdit.setId(commodityId);
            buttonEdit.setBackgroundColor(Color.WHITE);
            buttonEdit.setTextColor(Color.BLUE);
            LayoutParams paramsEdit = new LayoutParams(100, LayoutParams.WRAP_CONTENT);
            buttonEdit.setLayoutParams(paramsEdit);
            buttonEdit.setText("Edit");
            buttonEdit.setTextSize(12);
            buttonEdit.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    int cId = v.getId();
                    LoadCommodityForEdit(cId);
                }
            });
            row.addView(buttonEdit);

            //Create Delete Button
            Button buttonDelete = new Button(this);
            buttonDelete.setId(commodityId);
            buttonDelete.setBackgroundColor(Color.WHITE);
            buttonDelete.setTextColor(Color.RED);
            LayoutParams paramsDel = new LayoutParams(150, LayoutParams.WRAP_CONTENT);
            buttonDelete.setLayoutParams(paramsDel);
            buttonDelete.setText(" Delete");
            buttonDelete.setTextSize(12);
            buttonDelete.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    int cId = v.getId();
                    sqlController.open();
                    sqlController.deleteDataById(cId);
                    sqlController.close();
                    View row = (TableRow) v.getParent();
                    table_layout.removeView(row);
                    showtoast("Commodity with Id " + cId + " Deleted.");
                }
            });
            row.addView(buttonDelete);

            c.moveToNext();
            table_layout.addView(row);
        }
        sqlController.close();
    }

    private void LoadCommodityForEdit(int commodityId) {
        sqlController.open();
        Commodity c =  sqlController.getCommodityById(commodityId);
        sqlController.close();
        ETcommodityName.setText(c.get_commodityName());
        ETquantity.setText(c.get_quantity());
        comIdForUpdate = commodityId;
    }


    public void selectUserGroup(){
        List<String> users = this.getNames();
        spinner = (Spinner) findViewById(R.id.user_spinner);
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, users);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                spinner.setSelection(position);
                selectedUser = (String) spinner.getSelectedItem();

                setId(nameIdMapper.get(selectedUser));

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }
        });
    }

    public void selectOrganisationUnit(){
        List<String> users = this.getNames();
        spinner = (Spinner) findViewById(R.id.user_spinner);
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, users);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                spinner.setSelection(position);
                selectedUser = (String) spinner.getSelectedItem();

                setId(nameIdMapper.get(selectedUser));
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }
        });
    }

    public List<String> getNames() {
        return names;
    }

    public void setNames(List<String> names) {
        this.names = names;
    }

    public Map<String, String> getNameIdMapper() {
        return nameIdMapper;
    }

    public void setNameIdMapper(Map<String, String> nameIdMapper) {
        this.nameIdMapper = nameIdMapper;
    }

    public String getRecipientType() {
        return recipientType;
    }

    public void setRecipientType(String recipientType) {
        this.recipientType = recipientType;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


}
