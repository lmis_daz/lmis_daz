package daz.lmis_light;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

/**
 * A login screen that offers login via email/password.
 */

public class LoginActivity extends Activity {


    EditText username, password, url;
    String getUsername, getPassword, getUrl;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_main);
        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        url = (EditText) findViewById(R.id.urlBox);

    }

    public void btn_loginClicked(View v) {

        getUsername = username.getText().toString().trim();
        getPassword = password.getText().toString().trim();
        getUrl = url.getText().toString().trim();

        if(getUsername.isEmpty() || getPassword.isEmpty()){
            if(getUsername.isEmpty() && getPassword.isEmpty())
                showMessage("Please Enter", "Username and Password");
            else if(getUsername.isEmpty())
                showMessage("Please Enter", "Username");//show message
            else if(getPassword.isEmpty())showMessage("Please Enter", "Password");
            else{}
        }
        try {

            HttpLoginClient loginClient = new HttpLoginClient();
            JSONResponse response = loginClient.execute(getUsername, getPassword, getUrl).get();
            if(response.getStatus() == 200)
            {
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                intent.putExtra("user", getUsername);
                intent.putExtra("password",getPassword);
                startActivity(intent);
                showtoast("Login Successful");

            }
            else
            {
                showMessage("Message","Incorrect Login");
            }
        }catch (Exception e)
        {

        }
    }

    public void showtoast(String message)
    {
        Toast toast = Toast.makeText(this, message, Toast.LENGTH_SHORT);
        toast.show();
    }

    public void showMessage(String title, String Message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(Message);
        builder.show();
    }
}